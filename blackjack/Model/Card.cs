﻿namespace BlackJack.Model
{
    public class Card
    {
        public string Name { get; set; }
        public Suit Suit { get; set; }

        public Card(string name, Suit suit)
        {
            Name = name;
            Suit = suit;
        }

        public override string ToString()
        {
            return Name + " " + Suit;
        }
    }
}