﻿namespace BlackJack.Model
{
    public enum Suit
    {
        Clubs = 1,
        Peaks = 2,
        Diamonds = 3,
        Hearts = 4
    }
}