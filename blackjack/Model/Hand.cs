﻿using System.Collections.Generic;

namespace BlackJack.Model
{
    public class Hand
    {
        public List<Card> Cards = new List<Card>();

        public void AddCard(Card card)
        {
            if (card == null || Cards.Contains(card))
                return;

            Cards.Add(card);
        }
    }
}
