﻿using System;
using System.Collections.Generic;

namespace BlackJack.Model
{
    public class Deck
    {
        public readonly List<Card> Cards = new List<Card>();
        public static string[] Names = new string[13];
        
        public Deck()
        {
            FillDeck();
        }

        private void FillDeck()
        {
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                for (int i = 2; i <= 14; i++)
                {
                    switch (i)
                    {
                        case 11: Cards.Add(new Card("Jack", suit));
                            break;
                        case 12: Cards.Add(new Card("Queen", suit));
                            break;
                        case 13: Cards.Add(new Card("King", suit));
                            break;
                        case 14: Cards.Add(new Card("Ace", suit));
                            break;
                        default: Cards.Add(new Card(i.ToString(), suit));
                            break;
                    }
                }
            }
        }
    }
}