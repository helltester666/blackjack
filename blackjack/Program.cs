﻿using BlackJack.Controller;

namespace BlackJack
{
    class Program
    {
        static void Main(string[] args)
        {
            GameController game = new GameController();

            game.StartGame();
            do
            {
                game.PickCommand();
                game.PickNextRoundOption();
            } while (!game.IsEnd && !game.IsNextRoundStart.HasValue);
        }
    }
}
