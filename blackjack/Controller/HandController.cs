﻿using System;
using System.Collections.Generic;
using BlackJack.Model;

namespace BlackJack.Controller
{
    public class HandController
    {
        private readonly List<Card> _cards;
        public int Points { get; set; }

        public HandController(Hand hand)
        {
            _cards = hand.Cards;
            Points = 0;
        }
        
        public void CountPoints()
        {
            int points = 0;
            var aces = _cards.FindAll(c => c.Name.Contains("Ace"));

            var rest = _cards.FindAll(c => !c.Name.Contains("Ace"));
            rest.ForEach(c => points += GetCardPoints(c));

            aces.ForEach(c => points += points + 11 <= 21 ? 11 : 1);
            Points = points;
        }

        public List<Card> GetCards()
        {
            return _cards;
        }

        private int GetCardPoints(Card card)
        {
            int points = 0;
            if (Int32.TryParse(card.Name, out points))
            {
                return points;
            }

            return 10;
        }

        public void AddCard(Card card)
        {
            if (card == null || _cards.Contains(card))
                return;

            _cards.Add(card);
            CountPoints();
        }

        public bool IsLooser()
        {
            return Points > 21;
        }

        public bool IsAutomaticWin()
        {
            return Points == 21;
        }
    }
}
