﻿using System;
using BlackJack.Model;

namespace BlackJack.Controller
{
    public class DeckController
    {
        private readonly Deck _deck;

        public DeckController(Deck deck)
        {
            _deck = deck;
        }

        private int GetRestCardsNum()
        {
            return _deck.Cards.Count;
        }

        public Card ExtractCard()
        {
            if (GetRestCardsNum() == 0)
                return null;

            var random = new Random();
            var randomInt = random.Next(0, _deck.Cards.Count);
            var card = _deck.Cards[randomInt];
            _deck.Cards.RemoveAt(randomInt);
            return card;
        }
    }
}
