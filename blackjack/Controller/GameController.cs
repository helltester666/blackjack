﻿using System;
using BlackJack.Model;

namespace BlackJack.Controller
{
    public class GameController
    {
        public Deck Deck { get; set; }
        public DeckController DeckController { get; set; }
        private Hand DealerHand { get; set; }
        private Hand ClientHand { get; set; }

        public HandController DealerHandController { get; set; }
        public HandController ClientHandController { get; set; }

        public bool IsEnd { get; set; }
        public bool? IsNextRoundStart { get; set; }

        public void StartGame()
        {
            Deck = new Deck();
            DeckController = new DeckController(Deck);

            DealerHand = new Hand();
            ClientHand = new Hand();
            DealerHandController = new HandController(DealerHand);
            ClientHandController = new HandController(ClientHand);

            IsEnd = false;
            IsNextRoundStart = null;

            for (int i = 0; i < 2; i++)
            {
                ExtractCardToClient();
                ExtractCardToDealer();
            }
        }

        public void ExtractCardToClient()
        {
            var card = DeckController.ExtractCard();
            ClientHandController.AddCard(card);
            GetGameInfo();
            if (ClientHandController.IsLooser())
            {
                EndGame();
                IsEnd = true;
            }

            if (ClientHandController.IsAutomaticWin())
            {
                Congratulations();
                IsEnd = true;
            }
        }

        public void ExtractCardToDealer()
        {
            var card = DeckController.ExtractCard();
            DealerHandController.AddCard(card);
            GetGameInfo();
            if (DealerHandController.IsLooser())
            {
                Congratulations();
            }
        }

        private void GetGameInfo()
        {
            Console.Clear();
            Console.WriteLine("Dealer has these cards in his hand:");
            DealerHand.Cards.ForEach(c =>
            {
                Console.WriteLine($"\t {c}");
            });
            Console.WriteLine($"\t Total points: {DealerHandController.Points}");

            Console.WriteLine("You have these cards in a hand:");
            ClientHand.Cards.ForEach(c =>
            {
                Console.WriteLine($"\t {c}");
            });
            Console.WriteLine($"\t Total points: {ClientHandController.Points}\n");
        }

        private void HoldCards()
        {
            while (DealerHandController.Points <= 17)
            {
                ExtractCardToDealer();
            }

            if (!IsEnd)
            {
                if (DealerHandController.Points <= ClientHandController.Points)
                {
                    Congratulations();
                }
                else
                {
                    EndGame();
                }
            }
        }

        private void Congratulations()
        {
            Console.WriteLine($"\t\t\tCongratulations, you win this game!");
            IsEnd = true;
        }

        private void EndGame()
        {
            Console.WriteLine($"\t\t\tSorry, try to win next time...");
            IsEnd = true;
        }

        public void PickCommand()
        {
            Console.WriteLine("Following commands are availabe:\n\t- Take\n\t- Hold\n\t- End");
            string command = Console.ReadLine();
            switch (command.ToLower())
            {
                case "take": ExtractCardToClient();
                    break;
                case "hold": HoldCards();
                    break;
                case "end": IsEnd = true;
                    break;
                default: GetGameInfo();
                    break;
            }
        }

        public void PickNextRoundOption()
        {
            if (!IsEnd)
            {
                return;
            }

            do
            {
                Console.WriteLine($"\t\t\tPlay next round? [Y/n]");
                var ans = Console.ReadLine();
                if (ans.ToLower().Equals("n"))
                    IsNextRoundStart = false;
                if (ans.ToLower().Equals("y"))
                    IsNextRoundStart = true;
            } while (!IsNextRoundStart.HasValue);

            if (IsNextRoundStart.Value)
                StartGame();
        }
    }
}
