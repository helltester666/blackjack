﻿using System;
using System.Linq;
using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Model
{
    [TestFixture]
    public class DeckTests
    {
        private Deck _deck;

        [Test]
        public void TestConstructor_WhenCall_ShouldReturnWholeDeck()
        {
            _deck = new Deck();
            
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                for (int i = 2; i <= 14; i++)
                {
                    Card card;
                    switch (i)
                    {
                        case 11:
                            card = new Card("Jack", suit);
                            break;
                        case 12:
                            card = new Card("Queen", suit);
                            break;
                        case 13:
                            card = new Card("King", suit);
                            break;
                        case 14:
                            card = new Card("Ace", suit);
                            break;
                        default:
                            card = new Card(i.ToString(), suit);
                            break;
                    }

                    Assert.That(_deck.Cards.Any(c => c.Name == card.Name && c.Suit == card.Suit), Is.EqualTo(true));
                }
            }

            Assert.That(_deck.Cards.Count, Is.EqualTo(52));
        }
    }
}
