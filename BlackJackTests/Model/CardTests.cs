﻿using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Model
{
    [TestFixture]
    public class CardTests
    {
        private Card _card;

        [Test]
        public void TestToString_WhenCall_ShouldReturnNumAndSuit()
        {
            _card = new Card("Queen", Suit.Diamonds);
            Assert.That(_card.ToString(), Is.EqualTo("Queen Diamonds"));
        }

        [Test]
        public void TestToString_WhenCallNullName_ShouldReturnEmptyName()
        {
            _card = new Card(null, Suit.Peaks);
            Assert.That(_card.ToString(), Is.EqualTo(" Peaks"));
        }
    }
}
