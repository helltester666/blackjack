﻿using System.Linq;
using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Model
{
    [TestFixture]
    public class HandTests
    {
        private Hand _hand;

        [Test]
        public void TestAddCard_WhenPassNull_ShouldReturnSameHand()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("10", Suit.Hearts));

            _hand.AddCard(null);

            Assert.That(_hand.Cards.Count, Is.EqualTo(1));
            Assert.That(_hand.Cards.Any(c => c.Name == "10" && c.Suit == Suit.Hearts), Is.EqualTo(true));
        }

        [Test]
        public void TestAddCard_WhenPassCorrect_ShouldReturnOk()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("10", Suit.Hearts));

            Assert.That(_hand.Cards.Count, Is.EqualTo(1));
            Assert.That(_hand.Cards.Any(c => c.Name == "10" && c.Suit == Suit.Hearts), Is.EqualTo(true));
        }

        [Test]
        public void TestAddCard_WhenPassDuplicate_ShouldReturnSameHand()
        {
            _hand = new Hand();
            var card = new Card("10", Suit.Hearts);
            _hand.AddCard(card);
            _hand.AddCard(card);

            Assert.That(_hand.Cards.Count, Is.EqualTo(1));
            Assert.That(_hand.Cards.Any(c => c.Name == "10" && c.Suit == Suit.Hearts), Is.EqualTo(true));
        }
    }
}
