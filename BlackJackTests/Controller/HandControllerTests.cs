﻿using System;
using System.Collections.Generic;
using BlackJack.Controller;
using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Controller
{
    [TestFixture]
    class HandControllerTests
    {
        private HandController _controller;
        private Hand _hand;

        static object[] nonImagedCardPoints =
        {
            new object[] { "2", 2 },
            new object[] { "3", 3 },
            new object[] { "4", 4 },
            new object[] { "5", 5 },
            new object[] { "6", 6 },
            new object[] { "7", 7 },
            new object[] { "8", 8 },
            new object[] { "9", 9 },
            new object[] { "10", 10 },
            new object[] { "Jack", 10 },
            new object[] { "Queen", 10 },
            new object[] { "King", 10 },
            new object[] { "Ace", 11 }
        };

        private static object[] loserPoints =
        {
            new object[] { 21, false},
            new object[] { 22, true}
        };

        private static object[] autoWin =
        {
            new object[] { 21, true},
            new object[] { 22, false},
            new object[] { 20, false}
        };

        [Test]
        public void TestConstructor_WhenCreateWithNullHand_ShouldReturnNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => new HandController(null));
        }

        [Test]
        public void TestConstructor_WhenCreateEmptyHand_ShouldReturnEmptyList()
        {
            _hand = new Hand();

            _controller = new HandController(_hand);
            Assert.That(_controller.GetCards(), Is.EqualTo(new List<Card>()));
            Assert.That(_controller.Points, Is.EqualTo(0));
        }

        [Test]
        public void TestConstructor_WhenCreateHandWithCards_ShouldReturnListOfCards()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("Jack", Suit.Clubs));
            _hand.AddCard(new Card("5", Suit.Hearts));

            _controller = new HandController(_hand);
            Assert.That(_controller.GetCards().Count, Is.EqualTo(2));
            Assert.That(_controller.GetCards(), Is.EqualTo(_hand.Cards));
            Assert.That(_controller.Points, Is.EqualTo(0));
        }

        [Test, TestCaseSource("nonImagedCardPoints")]
        public void TestCountPoints_WhenPassNonImagedCard_ShouldReturnItsNumber(string name, int points)
        {
            _hand = new Hand();
            _hand.AddCard(new Card(name, Suit.Diamonds));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(points));
        }

        [Test]
        public void TestCountPoints_WhenPassImageAndAce_ShouldReturnCorrectSum()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("Ace", Suit.Diamonds));
            _hand.AddCard(new Card("Queen", Suit.Diamonds));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(21));
        }

        [Test]
        public void TestCountPoints_WhenPassOverheadAce_ShouldCountAs1Point()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("10", Suit.Clubs));
            _hand.AddCard(new Card("8", Suit.Diamonds));
            _hand.AddCard(new Card("3", Suit.Peaks));
            _hand.AddCard(new Card("Ace", Suit.Hearts));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(22));
        }

        [Test]
        public void TestCountPoints_WhenPassAceTo11PlusHand_ShouldCountAs1Point()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("9", Suit.Clubs));
            _hand.AddCard(new Card("2", Suit.Peaks));
            _hand.AddCard(new Card("Ace", Suit.Hearts));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(12));
        }

        [Test]
        public void TestCountPoints_WhenPass2Aces_ShouldReturn12Points()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("Ace", Suit.Hearts));
            _hand.AddCard(new Card("Ace", Suit.Clubs));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(12));
        }

        [Test]
        public void TestCountPoints_WhenPass4Aces_ShouldReturn14Points()
        {
            _hand = new Hand();
            _hand.AddCard(new Card("Ace", Suit.Hearts));
            _hand.AddCard(new Card("Ace", Suit.Clubs));
            _hand.AddCard(new Card("Ace", Suit.Peaks));
            _hand.AddCard(new Card("Ace", Suit.Diamonds));
            _controller = new HandController(_hand);
            _controller.CountPoints();

            Assert.That(_controller.Points, Is.EqualTo(14));
        }

        [Test]
        public void TestAddCard_WhenPassNull_ShouldReturnSameHand()
        {
            _hand = new Hand();
            _controller = new HandController(_hand);
            var oldCount = _controller.GetCards().Count;

            _controller.AddCard(null);

            Assert.That(_controller.GetCards().Count, Is.EqualTo(oldCount));
            Assert.That(_controller.Points, Is.EqualTo(0));
        }

        [Test]
        public void TestAddCard_WhenPassSeveralCards_ShouldReturnAllOfThem()
        {
            _hand = new Hand();
            _controller = new HandController(_hand);
            var oldCount = _controller.GetCards().Count;

            var firstCard = new Card("4", Suit.Clubs);
            var secondCard = new Card("King", Suit.Peaks);
            _controller.AddCard(firstCard);
            _controller.AddCard(secondCard);

            Assert.That(_controller.GetCards().Count, Is.EqualTo(oldCount + 2));
            Assert.That(_controller.GetCards(), Has.Member(firstCard));
            Assert.That(_controller.GetCards(), Has.Member(secondCard));
            Assert.That(_controller.Points, Is.EqualTo(14));
        }

        [Test]
        public void TestAddCard_WhenPassDuplicatedCards_ShouldReturnSameHand()
        {
            _hand = new Hand();
            _controller = new HandController(_hand);
            var oldCount = _controller.GetCards().Count;

            var firstCard = new Card("4", Suit.Clubs);
            _controller.AddCard(firstCard);
            _controller.AddCard(firstCard);

            Assert.That(_controller.GetCards().Count, Is.EqualTo(oldCount + 1));
            Assert.That(_controller.GetCards(), Has.Member(firstCard));
            Assert.That(_controller.GetCards(), Has.Member(firstCard));
            Assert.That(_controller.Points, Is.EqualTo(4));
        }

        [Test, TestCaseSource("loserPoints")]
        public void TestIsLooser_WhenPassPoints_ShouldReturnBool(int points, bool result)
        {
            _hand = new Hand();
            _controller = new HandController(_hand);
            _controller.Points = points;

            Assert.That(_controller.IsLooser, Is.EqualTo(result));
        }

        [Test, TestCaseSource("autoWin")]
        public void TestIsAutowin_WhenPassPoints_ShouldReturnBool(int points, bool result)
        {
            _hand = new Hand();
            _controller = new HandController(_hand);
            _controller.Points = points;

            Assert.That(_controller.IsAutomaticWin, Is.EqualTo(result));
        }
    }
}
