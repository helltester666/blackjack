﻿using BlackJack.Controller;
using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Controller
{
    [TestFixture]
    class GameControllerTests
    {
        private GameController _controller;

        [SetUp]
        public void SetUp()
        {
            _controller = new GameController();
            _controller.StartGame();
        }

        [Test]
        public void TestStartGame_WhenCall_ShouldStartGame()
        {
            Assert.That(_controller.Deck.Cards.Count, Is.EqualTo(48));
            Assert.That(_controller.ClientHandController.GetCards().Count, Is.EqualTo(2));
            Assert.That(_controller.DealerHandController.GetCards().Count, Is.EqualTo(2));
            Assert.That(_controller.IsEnd, Is.EqualTo(false));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToClient_WhenExtractOne_ShouldHaveNormalState()
        {
            _controller.ClientHandController.GetCards().Clear();
            _controller.ExtractCardToClient();
            
            Assert.That(_controller.Deck.Cards.Count, Is.EqualTo(47));
            Assert.That(_controller.ClientHandController.GetCards().Count, Is.EqualTo(1));
            Assert.That(_controller.ClientHandController.IsLooser, Is.EqualTo(false));
            Assert.That(_controller.ClientHandController.IsAutomaticWin, Is.EqualTo(false));
            Assert.That(_controller.IsEnd, Is.EqualTo(false));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToClient_WhenExtractMany_ShouldLose()
        {
            for (int i = 0; i < 10; i++)
            {
                _controller.ExtractCardToClient();
            }

            Assert.That(_controller.ClientHandController.IsLooser, Is.EqualTo(true));
            Assert.That(_controller.ClientHandController.IsAutomaticWin, Is.EqualTo(false));
            Assert.That(_controller.IsEnd, Is.EqualTo(true));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToClient_WhenGet21_ShouldAutoWin()
        {
            _controller.ClientHandController.GetCards().Clear();

            _controller.Deck.Cards.Clear();
            _controller.Deck.Cards.Add(new Card("10", Suit.Clubs));
            _controller.Deck.Cards.Add(new Card("Ace", Suit.Clubs));

            _controller.ExtractCardToClient();
            _controller.ExtractCardToClient();

            Assert.That(_controller.ClientHandController.IsLooser, Is.EqualTo(false));
            Assert.That(_controller.ClientHandController.IsAutomaticWin, Is.EqualTo(true));
            Assert.That(_controller.IsEnd, Is.EqualTo(true));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToDealer_WhenExtractOne_ShouldHaveNormalState()
        {
            _controller.DealerHandController.GetCards().Clear();
            _controller.ExtractCardToDealer();

            Assert.That(_controller.Deck.Cards.Count, Is.EqualTo(47));
            Assert.That(_controller.DealerHandController.GetCards().Count, Is.EqualTo(1));
            Assert.That(_controller.DealerHandController.IsLooser, Is.EqualTo(false));
            Assert.That(_controller.DealerHandController.IsAutomaticWin, Is.EqualTo(false));
            Assert.That(_controller.IsEnd, Is.EqualTo(false));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToDealer_WhenExtractMany_ShouldLose()
        {
            for (int i = 0; i < 10; i++)
            {
                _controller.ExtractCardToDealer();
            }

            Assert.That(_controller.DealerHandController.IsLooser, Is.EqualTo(true));
            Assert.That(_controller.DealerHandController.IsAutomaticWin, Is.EqualTo(false));
            Assert.That(_controller.IsEnd, Is.EqualTo(true));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }

        [Test]
        public void TestExtractToDealer_WhenGet21_ShouldNotAutoWin()
        {
            _controller.DealerHandController.GetCards().Clear();

            _controller.Deck.Cards.Clear();
            _controller.Deck.Cards.Add(new Card("10", Suit.Clubs));
            _controller.Deck.Cards.Add(new Card("Ace", Suit.Clubs));

            _controller.ExtractCardToDealer();
            _controller.ExtractCardToDealer();

            Assert.That(_controller.DealerHandController.IsLooser, Is.EqualTo(false));
            Assert.That(_controller.DealerHandController.IsAutomaticWin, Is.EqualTo(true));
            Assert.That(_controller.IsEnd, Is.EqualTo(false));
            Assert.That(_controller.IsNextRoundStart, Is.Null);
        }
    }
}
