﻿using BlackJack.Controller;
using BlackJack.Model;
using NUnit.Framework;

namespace BlackJackTests.Controller
{
    [TestFixture]
    public class DeckControllerTests
    {
        private DeckController _controller;
        private Deck _deck;

        //я могу вытащить все карты
        [Test]
        public void TestExtract_WhenExtractFromNull_ShouldReturnNull()
        {
            _deck = new Deck();
            _controller = new DeckController(_deck);

            while (_deck.Cards.Count != 0)
            {
                _deck.Cards.RemoveAt(0);
            }

            Assert.That(_controller.ExtractCard(), Is.Null);
        }

        [Test]
        public void TestExtract_WhenExtractCorrect_ShouldReturnExactThisCard()
        {
            _deck = new Deck();
            _controller = new DeckController(_deck);

            var card = _controller.ExtractCard();

            Assert.That(_deck.Cards.Count, Is.EqualTo(51));
            Assert.That(_deck.Cards, Has.No.Member(card));
        }

        [Test]
        public void TestExtract_WhenExtractAllDeck_ShouldReturnEmptyDeck()
        {
            _deck = new Deck();
            _controller = new DeckController(_deck);

            for (int i = 0; i < 52; i++)
            {
                _controller.ExtractCard();
            }

            Assert.That(_deck.Cards.Count, Is.EqualTo(0));
        }

        [Test]
        public void TestExtract_WhenExtractSeveralTimes_ShouldReturnRandomCard()
        {
            _deck = new Deck();
            _controller = new DeckController(_deck);

            var firstCard = _controller.ExtractCard();

            _deck = new Deck();
            _controller = new DeckController(_deck);

            var secondCard = _controller.ExtractCard();
            
            Assert.That(firstCard, Is.Not.EqualTo(secondCard));
        }
    }
}
